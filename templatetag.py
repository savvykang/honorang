#encoding=utf8
from datetime import datetime, timedelta

def humanize_kr(dt):
    def minutes(delta):
        return (delta.seconds // 60) % 60
    def hours(delta):
        return delta.seconds // 3600
    now = datetime.now()
    diff = now - dt
    if diff < timedelta(minutes=1):
        return u'지금'
    elif diff < timedelta(hours=1):
        return u'{}분 전'.format(minutes(diff))
    elif now.day == dt.day:
        if hours(diff) < 3:
            return u'{}시간 전'.format(hours(diff))
        else:
            return u'{} {}시 {}'.format(u'오전' if dt.hour < 12 else u'오후',
                                          dt.hour,
                                          u'' if dt.minute == 0 else u'{}분'.format(dt.minute))
    elif now.year == dt.year:
        return u'{}월 {}일'.format(dt.month, dt.day)
    else:
        return u'{}년 {}월 {}일'.format(dt.year, dt.month, dt.day)

def humanize_time(dt):
    return u'{} {}시 {}'.format(u'오전' if dt.hour < 12 else u'오후',
                                          dt.hour,
                                          u'' if dt.minute == 0 else u'{}분'.format(dt.minute))

print humanize_time(datetime.now()-timedelta(hours=6, minutes=3))
