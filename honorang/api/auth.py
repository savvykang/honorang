from tastypie.authentication import Authentication

class WebAuthentication(Authentication):
    def is_authenticated(self, request, **kwargs):
        return request.method == 'GET' or request.user.is_authenticated()

    def get_identifier(self, request):
        return request.user.username
        
