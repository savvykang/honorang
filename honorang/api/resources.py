from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authentication import Authentication
from tastypie.authorization import Authorization
from tastypie.api import Api as API
from tastypie.validation import FormValidation, CleanedDataFormValidation

from honorang.forum.models import Post, Profile, Photo, Event, Reservation
from honorang.api.auth import WebAuthentication
from honorang.forum.forms import ReservationForm

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        excludes = 'email', 'password', 'is_superuser', 'is_staff', 'is_active', 'first_name', 'last_name'
        list_allowed_methods = ()
        
    def dehydrate(self, bundle):
        return bundle

class ProfileResource(ModelResource):
    user = fields.ToOneField(UserResource, 'user', full=True)
    class Meta:
        queryset = Profile.objects.all()
        list_allowed_methods = ()

class PostResource(ModelResource):
    author = fields.ToOneField(ProfileResource, 'author', full=True)
    edited_at = fields.DateTimeField(attribute='edited_at', blank=True, null=True)
    posted_at = fields.DateTimeField(attribute='posted_at', blank=True, null=True)
    class Meta:
        queryset = Post.objects.all()
        authentication = WebAuthentication()
        authorization = Authorization()

class PhotoResource(PostResource):
    class Meta:
        queryset = Photo.objects.all()


class EventResource(PostResource):
    class Meta:
        queryset = Event.objects.all()

class ReservationResource(PostResource):
    class Meta:
        queryset = Reservation.objects.all()
        authentication = WebAuthentication()
        authorization = Authorization()
        validation = CleanedDataFormValidation(form_class=ReservationForm)
