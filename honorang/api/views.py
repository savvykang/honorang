# Create your views here.
from django.contrib import auth
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt

from tastypie.api import Api as API

from honorang.api.resources import (UserResource, PostResource, ProfileResource,
                                    PhotoResource, EventResource, ReservationResource)
from honorang.forum.models import Post
from honorang.middleware import (renderjson as json,
                                 require_http_methods)
from honorang.util import router

urlpatterns, route = router()

@route(r'login$', csrf_protect=False)
@require_http_methods('POST')
@json
def login(request):
    response = {}
    id = request.POST["username"]
    password = request.POST["password"]
    user = auth.authenticate(username=id, password=password)
    if user is not None:
        auth.login(request, user)
        response["logged_in"] = True
    else:
        response["logged_in"] = False
    response.update(request.COOKIES)
    response['sessionid'] = request.session.session_key
    response['next_url'] = request.POST.get('next_url', '/')
    return response

@route(r'logout$')
@require_http_methods('POST')
@json
def logout(request):
    print request.POST
    auth.logout(request)
    response = {}
    response['logged_in'] = False 
    response['next_url'] = request.POST.get('next_url', '/')
    return response

api = API(api_name='v1')
api.register(UserResource(), canonical=True)
api.register(PostResource(), canonical=True)
api.register(ProfileResource(), canonical=True)
api.register(PhotoResource(), canonical=True)
api.register(EventResource(), canonical=True)
api.register(ReservationResource(), canonical=True)

urlpatterns += api.urls
