from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse
from honorang.forum.forms import PostForm
from honorang.forum.models import Menu
from honorang.util import mc_set
import memcache
mc = memcache.Client(['127.0.0.1:11211'], debug=0)

def layout(request):
    nav = mc.get('nav') or mc_set(mc, 'nav', [dict(name=menu.name, url=reverse(menu.view), divider=menu.divider, view=str(menu.view)) for menu in Menu.objects.all()])
    login_form = mc.get('login_form') or mc_set(mc, 'login_form', AuthenticationForm())
    post_form = PostForm()
    return locals()

