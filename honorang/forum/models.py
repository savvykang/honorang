from django.db import models
from django.contrib import auth
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage as s3_storage
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from polymorphic import PolymorphicModel

from honorang.middleware import register_admin
from honorang.util import truncate, validate_range

@register_admin
class Attachment(models.Model):
    posted_at = models.DateTimeField(auto_now_add=True)

@register_admin
class Image(Attachment):
    image = models.ImageField(storage=s3_storage, upload_to='/')

    def __unicode__(self):
        return self.image.url

@register_admin
class Profile(models.Model):
    user = models.OneToOneField(auth.models.User)
    name = models.CharField(max_length=20)
    photo = models.URLField(blank=True)
    job = models.CharField(max_length=40, blank=True)
    contact = models.CharField(max_length=20, blank=True)
    signature = models.TextField(blank=True)

    def __unicode__(self):
        user = self.user
        return u'{} ({})'.format(self.name, user.username)

@register_admin
class Category(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name
    
@register_admin
class Post(PolymorphicModel):
    title = models.CharField(_('Title'), max_length=80, blank=True)
    content = models.TextField(_('Content'), default='')
    author = models.ForeignKey(Profile)
    posted_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)
    category = models.ManyToManyField(Category, blank=True)

    def __unicode__(self):
        return u'[{}] {} by {} @ {}'.format(self.__class__.__name__, truncate(self.title or self.content, 20), unicode(self.author), self.posted_at)

    class Meta:
        ordering = '-id',
        
@register_admin
class Event(Post):
    starts_at = models.DateTimeField()
    ends_at = models.DateTimeField(blank=True, null=True)

    def clean(self):
        super(Event, self).clean()
        validate_range(self.starts_at, self.ends_at)

@register_admin
class Photo(Post):
    image = models.ForeignKey(Image)

@register_admin
class Reservation(Post):
    starts_at = models.DateTimeField()
    ends_at = models.DateTimeField()
    class Meta:
        ordering = ("starts_at", )
    
    def clean(self):
        super(Reservation, self).clean()
        validate_range(self.starts_at, self.ends_at)
        # if A.start < B.end and B.start > A.end,
        #     then A and B overlaps
        overlapping = Reservation.objects.filter(~models.Q(id=self.id)).filter(starts_at__lt=self.ends_at).filter(ends_at__gt=self.starts_at)[:1]
        if overlapping:
            raise ValidationError("no overlapping date")

@register_admin
class Menu(models.Model):
    name = models.CharField(max_length=40)
    view = models.CharField(max_length=80, blank=True)
    divider = models.BooleanField()

    def __unicode__(self):
        if self.divider:
            return u'---'
        else:
            try:
                view = reverse(self.view)
            except:
                view = '<NO URL MATCHED>'
                
            return u"{0} - {1}".format(self.name, view)
