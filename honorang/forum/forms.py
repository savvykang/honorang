#encoding=utf-8
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import ugettext_lazy as _
from honorang.forum import models
from honorang.util import validate_range

class PostForm(forms.ModelForm):
    class Meta:
        model = models.Post
        fields = ('title', 'content', )

class PhotoForm(forms.ModelForm):
    pass

class ReservationForm(PostForm):
    class Meta:
        model = models.Reservation
        fields = ('title', 'content', 'starts_at', 'ends_at')

    def clean(self):
        super(ReservationForm, self).clean()
        return self.cleaned_data

class JoinForm(UserCreationForm):
    name = forms.CharField(label=_("Name"), max_length=30)
    username = forms.RegexField(label=_("Account"), max_length=30,
        regex=r'^[\w.@+-]+$',
        help_text = _("Letters, digits and @/./+/-/_ only."),
        error_messages = {
            'invalid': _("This value may contain only letters, numbers and "
                         "@/./+/-/_ characters.")})
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text = _("Enter the same password as above."))

    email = forms.EmailField(widget=forms.TextInput(attrs=dict({'class': 'required'},
                                                               maxlength=75)),
                             label=_("E-mail"))

    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.
        
        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']

    def save(self, commit=True):
        user = super(JoinForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
            models.Profile(user=user, name=self.cleaned_data["name"]).save()

        return user
    
    class Meta:
        fields = ("name", "email", "username",)
        model = User
