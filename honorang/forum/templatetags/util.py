#encoding=utf8
from datetime import datetime
from django import template
from honorang.util import chunks
from django.utils import timezone

register = template.Library()
register.filter('chunks', chunks)

from datetime import datetime, timedelta

@register.filter
def relative(dt):
    dt, _dt = dt.astimezone(timezone.get_current_timezone()), dt
    def minutes(delta):
        return (delta.seconds // 60) % 60
    def hours(delta):
        return delta.seconds // 3600
    now = timezone.now()
    diff = now - dt
    if diff < timedelta(minutes=1):
        return u'방금'
    elif diff < timedelta(hours=1):
        return u'{}분 전'.format(minutes(diff))
    elif now.day == dt.day:
        if hours(diff) < 3:
            return u'{}시간 전'.format(hours(diff))
        else:
            return time(_dt)
        
    else:
        return date(_dt)
@register.filter
def time(d):
    d = d.astimezone(timezone.get_current_timezone())
    def _24_to_12(hour):
        if hour == 0:
            return 12
        elif hour > 12:
            return hour - 12
        else:
            return hour
        
    return u"{I} {hour}시 {min}".format(I=u"오전" if d.hour < 12 else u"오후",
                                        hour=_24_to_12(d.hour),
                                        min=u'' if d.minute == 0 else u'{}분'.format(d.minute))

@register.filter
def date(dt):
    now = timezone.now()
    if now.year == dt.year:
        return u'{}월 {}일'.format(dt.month, dt.day)
    else:
        return u'{}년 {}월 {}일'.format(dt.year, dt.month, dt.day)

@register.filter
def full(dt):
    return u' '.join([date(dt), time(dt)])
