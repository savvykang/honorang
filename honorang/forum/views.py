from calendar import monthrange
from datetime import datetime, timedelta

from django.conf.urls import url, patterns
from django.core.urlresolvers import reverse as _reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render, get_object_or_404
from django.utils import timezone

from honorang.util import router, chunks, mc_set, daterange
from honorang.middleware import require_http_methods, renderjson
from honorang.forum.forms import JoinForm, PostForm
from honorang.forum.models import Profile, Post, Reservation

from dateutil.relativedelta import relativedelta

import memcache
import requests

urlpatterns, route = router()
mc = memcache.Client(['127.0.0.1:11211'], debug=0)

def reverse(view=None, resource=None):
    if view:
        return _reverse(view)
    if resource:
        return _reverse('api_dispatch_list', kwargs=dict(resource_name=resource, api_name='v1'))

@route(r'^$')
def main(request):
    return render(request, 'main.html', {})

@route(r'^announcements$')
def announcements(request):
    if request.method == "GET":
        context = dict(objects=Post.objects.filter(category=1))
        return render(request, 'posts.html', context)

@route(r'^posts$')
def posts(request):
    if request.method == "GET":
        offset = request.GET.get("offset", None)
        limit = min(int(request.GET.get("limit", 16)), 100)
        
        if offset is not None:
            query_set = Post.objects.filter(id__lte=offset)
        else:
            query_set = Post.objects.all()
        objs = query_set[:limit]
        context = dict(objects=list(objs))
        context["current_nav"] = 'honorang.forum.views.posts'
        if objs:
            n = len(objs)
            
            latest = Post.objects.all()[0].id
            last = objs[0].id
            if last < latest:
                context["prev"] = min(last+int(limit), latest)

            oldest = Post.objects.reverse()[0].id
            first = objs[n-1].id
            if oldest < first:
                context["next"] = first-1
            
        return render(request, 'posts.html', context)
    
    elif request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user.profile
            post.save()
            return redirect('honorang.forum.views.posts')
        else:
            return None


@route(r'^posts/write$')
@require_http_methods('GET')
@login_required(login_url='/login')
def post_write(request):
    ctx = dict(form=PostForm())
    ctx["current_nav"] = 'honorang.forum.views.posts'
    return render(request, 'post_write.html', ctx)

@route(r'^posts/(\d+)')
def post_detail(request, post_id):
    p = get_object_or_404(Post, pk=int(post_id))
    ctx = dict(post=p)
    ctx["current_nav"] = 'honorang.forum.views.posts'
    return render(request, 'post_detail.html', ctx)

@route(r'^join$')
@require_http_methods('GET')
def join(request):
    ctx = {"form": mc.get('join_form') or mc_set(mc, 'join_form', JoinForm())}
    return render(request, 'join.html', ctx)

@route(r'^users$')
@require_http_methods('POST')
def users(request):
    form = JoinForm(request.POST)
    if form.is_valid():
        user = form.save()
        return render(request, 'registration/complete.html')
    else:
        return None

@route(r'^reservations$')
def reservations(request):
    def weeknumber(day):
        return (day - datetime(day.year, 1, 1, tzinfo=timezone.get_current_timezone())).days / 7 + 1
    today = timezone.now()
    year = int(request.GET.get("year", today.year))
    month = int(request.GET.get("month", today.month))

    first_day = datetime(year=year, month=month, day=1, tzinfo=timezone.get_current_timezone())
    start = first_day - timedelta(days=(first_day.weekday()+1)%7)
    
    ctx = dict()
    ctx["today"] = today
    ctx["offset"] = first_day
    ctx["current_nav"] = 'honorang.forum.views.reservations'

    last_day_in_month = first_day.replace(day=monthrange(year, month)[1])
    end = last_day_in_month + timedelta(days=6-(last_day_in_month.weekday()+1)%7)
    
    offset_week = weeknumber(first_day)
    end_week = weeknumber(last_day_in_month)

    week_diff = end_week - offset_week + 1
    if week_diff < 0:
        week_diff += 52

    __ww = list(Reservation.objects.filter(starts_at__gte=start).filter(ends_at__lt=end))

    def weeks(start, range):
        def __inner__(day):
            return dict(date=day,
                        reservations=[r for r in __ww if day <= r.starts_at and r.ends_at < (day + timedelta(days=1))])
        days = [__inner__(day) for day in daterange(range, offset=start)]
        return chunks(days, 7)

    ctx["weeks"] = weeks(start, timedelta(days=7*week_diff))
    ctx["prev"] = first_day - relativedelta(months=1)
    ctx["next"] = first_day + relativedelta(months=1)

    return render(request, 'reservations.html', ctx)

@route(r'^information$')
def information(request):
    ctx = dict(current_nav="honorang.forum.views.information")
    return render(request, 'information.html', ctx)


urlpatterns += patterns('', (r'^login$', 'django.contrib.auth.views.login', {"extra_context":{"current_nav": "django.contrib.auth.views.login"}}))
urlpatterns += patterns('', (r'^logout$', 'django.contrib.auth.views.logout', {"extra_context": {"current_nav": "django.contrib.auth.views.logout"}}))
