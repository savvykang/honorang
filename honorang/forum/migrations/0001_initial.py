# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Attachment'
        db.create_table('forum_attachment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('posted_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('forum', ['Attachment'])

        # Adding model 'Image'
        db.create_table('forum_image', (
            ('attachment_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['forum.Attachment'], unique=True, primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal('forum', ['Image'])

        # Adding model 'Profile'
        db.create_table('forum_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('photo', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
            ('job', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('contact', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('signature', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('forum', ['Profile'])

        # Adding model 'Post'
        db.create_table('forum_post', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('polymorphic_ctype', self.gf('django.db.models.fields.related.ForeignKey')(related_name='polymorphic_forum.post_set', null=True, to=orm['contenttypes.ContentType'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=80, blank=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default='')),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['forum.Profile'])),
            ('posted_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('edited_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('forum', ['Post'])

        # Adding model 'Announcement'
        db.create_table('forum_announcement', (
            ('post_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['forum.Post'], unique=True, primary_key=True)),
            ('starts_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('ends_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal('forum', ['Announcement'])

        # Adding model 'Photo'
        db.create_table('forum_photo', (
            ('post_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['forum.Post'], unique=True, primary_key=True)),
            ('image', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['forum.Image'])),
        ))
        db.send_create_signal('forum', ['Photo'])

        # Adding model 'Reservation'
        db.create_table('forum_reservation', (
            ('post_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['forum.Post'], unique=True, primary_key=True)),
            ('starts_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('ends_at', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('forum', ['Reservation'])

        # Adding model 'Menu'
        db.create_table('forum_menu', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('view', self.gf('django.db.models.fields.CharField')(max_length=80, blank=True)),
            ('divider', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('forum', ['Menu'])


    def backwards(self, orm):
        # Deleting model 'Attachment'
        db.delete_table('forum_attachment')

        # Deleting model 'Image'
        db.delete_table('forum_image')

        # Deleting model 'Profile'
        db.delete_table('forum_profile')

        # Deleting model 'Post'
        db.delete_table('forum_post')

        # Deleting model 'Announcement'
        db.delete_table('forum_announcement')

        # Deleting model 'Photo'
        db.delete_table('forum_photo')

        # Deleting model 'Reservation'
        db.delete_table('forum_reservation')

        # Deleting model 'Menu'
        db.delete_table('forum_menu')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'forum.announcement': {
            'Meta': {'ordering': "('-posted_at',)", 'object_name': 'Announcement', '_ormbases': ['forum.Post']},
            'ends_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'post_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Post']", 'unique': 'True', 'primary_key': 'True'}),
            'starts_at': ('django.db.models.fields.DateTimeField', [], {})
        },
        'forum.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'forum.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['forum.Attachment']},
            'attachment_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Attachment']", 'unique': 'True', 'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'forum.menu': {
            'Meta': {'object_name': 'Menu'},
            'divider': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'view': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'})
        },
        'forum.photo': {
            'Meta': {'ordering': "('-posted_at',)", 'object_name': 'Photo', '_ormbases': ['forum.Post']},
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.Image']"}),
            'post_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Post']", 'unique': 'True', 'primary_key': 'True'})
        },
        'forum.post': {
            'Meta': {'ordering': "('-posted_at',)", 'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.Profile']"}),
            'content': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'edited_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_forum.post_set'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'posted_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'})
        },
        'forum.profile': {
            'Meta': {'object_name': 'Profile'},
            'contact': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'photo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'signature': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'forum.reservation': {
            'Meta': {'ordering': "('-posted_at',)", 'object_name': 'Reservation', '_ormbases': ['forum.Post']},
            'ends_at': ('django.db.models.fields.DateTimeField', [], {}),
            'post_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Post']", 'unique': 'True', 'primary_key': 'True'}),
            'starts_at': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['forum']