# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table('forum_category', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('forum', ['Category'])

        # Adding M2M table for field category on 'Post'
        db.create_table('forum_post_category', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('post', models.ForeignKey(orm['forum.post'], null=False)),
            ('category', models.ForeignKey(orm['forum.category'], null=False))
        ))
        db.create_unique('forum_post_category', ['post_id', 'category_id'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table('forum_category')

        # Removing M2M table for field category on 'Post'
        db.delete_table('forum_post_category')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'forum.attachment': {
            'Meta': {'object_name': 'Attachment'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'posted_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'forum.category': {
            'Meta': {'object_name': 'Category'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'forum.event': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'Event', '_ormbases': ['forum.Post']},
            'ends_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'post_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Post']", 'unique': 'True', 'primary_key': 'True'}),
            'starts_at': ('django.db.models.fields.DateTimeField', [], {})
        },
        'forum.image': {
            'Meta': {'object_name': 'Image', '_ormbases': ['forum.Attachment']},
            'attachment_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Attachment']", 'unique': 'True', 'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'forum.menu': {
            'Meta': {'object_name': 'Menu'},
            'divider': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'view': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'})
        },
        'forum.photo': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'Photo', '_ormbases': ['forum.Post']},
            'image': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.Image']"}),
            'post_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Post']", 'unique': 'True', 'primary_key': 'True'})
        },
        'forum.post': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'Post'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['forum.Profile']"}),
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['forum.Category']", 'symmetrical': 'False'}),
            'content': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'edited_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'polymorphic_ctype': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'polymorphic_forum.post_set'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'posted_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '80', 'blank': 'True'})
        },
        'forum.profile': {
            'Meta': {'object_name': 'Profile'},
            'contact': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'photo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'signature': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'forum.reservation': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'Reservation', '_ormbases': ['forum.Post']},
            'ends_at': ('django.db.models.fields.DateTimeField', [], {}),
            'post_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['forum.Post']", 'unique': 'True', 'primary_key': 'True'}),
            'starts_at': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['forum']