import memcache

from datetime import datetime, timedelta

from django.conf.urls import patterns, url
from django.http import HttpResponse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt

from multimethod import multimethod

def router():
    _ = lambda x: x
    urlpatterns = patterns('')
    class route(object):
        def __init__(self, pattern, csrf_protect=True):
            self.pattern = pattern
            self.csrf_protect = csrf_protect
        def __call__(self, method):
            d = (_ if self.csrf_protect else csrf_exempt)(method)
            urlpatterns.append(url(self.pattern, d))
            return d
    return urlpatterns, route

def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def truncate(text, length):
    if len(text) > length:
        return u'{}...'.format(text[:length])
    else:
        return text

def mc_set(mc, key, value, *args):
    mc.set(key, value, *args)
    return value


def daterange(range, offset=None, step=None):
    if offset is None:
        offset = datetime.now()
    if step is None:
        step = timedelta(days=1)
        
    cursor = offset
    while cursor < offset + range:
        yield cursor
        cursor += step

def validate_range(start, end):
    if start and end and start > end:
        raise ValidationError('invalid range')
