from datetime import datetime
from django.utils.html import strip_tags
import requests
import random
import json
import string

cookies = {}

def login(username, password, cookies):
    params = dict(username=username, password=password)
    return requests.post('http://localhost:8000/api/login', cookies=cookies, data=params)

cred = login('kang', 'Specialk7&', cookies)
cookies.update(cred.cookies)

headers = {'Content-Type': 'application/json'}

def write(resource="post", **post):
    post['author'] = '/api/v1/profile/1'
    return requests.post('http://localhost:8000/api/v1/{}'.format(resource),
                         headers=headers,
                         data=json.dumps(post),
                         cookies=cookies)
'''
def send(content):
    day = random.randint(20, 30)
    hour = random.randint(0,21)
    starts_at = datetime(year=2012, month=9, day=day, hour=hour)
    ends_at = datetime(year=2012, month=9, day=day, hour=23)
    return write(resource="reservation",
                 content=content,
                 starts_at=starts_at.isoformat(),
                 ends_at=ends_at.isoformat())

for _ in xrange(1):
    content = ''.join(random.sample(string.letters, random.randint(5, len(string.letters))))
    print send(content).json["traceback"]

'''

doc = strip_tags(requests.get("http://guny.kr/stuff/klorem/text.php?count=100&type=p&truncate=0").content)

def send(contents):
    for content in contents:
        obj = dict(content=content)
        if random.random() < 0.7:
            obj["title"] = content[:10]
        write(**obj)

for i in xrange(100):   
    send(doc.split('\n\n'))
#send(str(i)+('_'*100) for i in xrange(100))
