#!/bin/bash

PYTHON_PATH=$(which python2.7)
MYSQL_PATH=$(find /usr -iname mysql_config 2>/dev/null | sed 's#/bin/mysql_config##')
GETTEXT_PATH=$(find /usr -iname xgettext 2>/dev/null | sed 's#/bin/xgettext##')

virtualenv -p $PYTHON_PATH .
echo "export DYLD_LIBRARY_PATH=$MYSQL_PATH/lib" >> bin/activate
sed -i.bak "s#$VIRTUAL_ENV/bin#$VIRTUAL_ENV/bin:$MYSQL_PATH/bin:$GETTEXT_PATH/bin#" bin/activate
source bin/activate
pip install -r requirements.txt
